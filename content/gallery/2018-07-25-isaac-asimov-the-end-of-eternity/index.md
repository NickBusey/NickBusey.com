---
title: "Isaac Asimov the End of Eternity"
date: 2018-07-25
categories:
  - Book Reviews
tags:
  - Sci-Fi
  - Fiction
aliases:
  - /gallery/isaac-asimov-the-end-of-eternity/
---

Isaac Asimov's "The End of Eternity" is an interesting exploration of the paradoxes of time travel, and what it might look like if there was a secret organization interfering with events throughout human history in order to acheive more 'desirable' outcomes.

This book is a classic, as evidenced by the sheer number of amazing retro covers I found with a quick image search.

Many of the classic time travel paradoxes arise like meeting yourself in the past, but due to the nature of the seemingly benevolent organization known as 'Eternity' there are some perhaps less thought of ideas. Like what happens if you change the course of time to where an entire war does or does not take place, millions of humans who did exist will cease to, and millions of others who did not exist, will suddenly exist.

The members of Eternity are known as the Eternals, and while they are not immortal, they are somewhat free to move around within a very wide range of time of human evolution, from the early 1900s to the 100,000,000s. The 'technicians' perform changes throughout time that have been decided would improve human circumstances overall by people referred to as 'computers'.

There is a council overseeing these decisions called the All-When council, but who is to say if what they decide is really for the best?

5/5