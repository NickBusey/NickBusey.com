---
title: "Live on Twitch"
date: 2019-04-10
categories:
  - Code
tags:
  - Programming
  - Open Source
  - Twitch
  - Live Stream

---

I have recently begun [streaming my open source work on Twitch.tv](https://www.twitch.tv/nickbusey)

It's been fun so far getting to interact with the viewers via the chatroom, and nice to have
scheduled, dedicated time to work on my various projects. It's much harder to just open a new
tab and start browsing HN when you have people watching you.

<!--more-->

I've been making pretty consistent progress both on my projects and with viewership numbers
since I started. So far I have been working primarily on my home brew beer tracking software 
[Mashio](https://gitlab.com/NickBusey/mashio),
but I plan to work on my knowledgebase app [BulletNotes](https://gitlab.com/NickBusey/BulletNotes),
my selfhosted data center project [HomelabOS](https://gitlab.com/NickBusey/HomelabOS), and 
my home inventory app [Inventario](https://gitlab.com/NickBusey/inventario) as well.

![Screenshot of Twitch Stream](screenshot.png)

Currently I am streaming Monday, Wednesday, Friday, 8-9AM MST. I do randomly jump on at other
times as well, and you can Follow me on Twitch to get Notifications when I do, if you're into
that sort of thing.

Someone asked me a while on reddit back to setup a [Patreon for my work](https://www.patreon.com/nickbusey)
on Inventario, and I have recently gained my first Patron, so I am now officially making a bit of money
back from my open source work!
From what I have seen of other open source contributors talking about their efforts, it's pretty
rare to get anything at all, so this is a nice unexpected bonus. I'm at $6/month so far, so not exactly
retirement worthy, but hey it's something.

I have been pretty impressed by the reaction so far actually, a bunch more people are tuning in then I
expected, and I'm excited to see hopefully a community of people who want to learn and create together
grow around the stream.

So come join us and say hi! [https://www.twitch.tv/nickbusey](https://www.twitch.tv/nickbusey)