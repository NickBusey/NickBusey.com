---
title: "Exterminate your desk: How to remove your mouse"
date: 2023-06-01
categories:
  - Tutorials

---

A few notes on how I finally got rid of my mouse.

## The why

Like many people, I spend the majority of my day working on a computer. I have taken several steps over the years in order to minimize the risk of things like [carpal tunnel](https://en.wikipedia.org/wiki/Carpal_tunnel_syndrome) and [RSI](https://en.wikipedia.org/wiki/Repetitive_strain_injury).

First I learned DVORAK to make typing more natural, I'm a huge fan of it, and being forced to use QWERTY again is never a fun experience. I can still type in QWERTY rather quickly, but it is no where near as pleasant.

Then, learning Vim well, and applying Vim extensions to my IDE and browser, allows me to navigate content with ease.

Next, I got a [split keyboard](https://ultimatehackingkeyboard.com/) that allows negative tilting. As I'm a rather large person with wide shoulders, this is a game changer in daily comfort as well.

My final, and perhaps most impactful step was removing my mouse entirely.

Moving your hand from the keyboard to the mouse and back, is not only a large cause of RSI, but takes more time than you think, and definitely slows me down.

## Taking the plunge

I haven't had a mouse on my work desk for all of 2023, and don't plan to ever add one back. How do I do this? A combination of three things.

Quality window management software, quality shortcut software, and mouse keys.

## Window Management

One of my main reasons to have to reach for the mouse is to manage the position and size of the windows on your screen.

Using any decent window management software that offers keyboard shortcuts fixes this. I'm in macOS land, so a combination of [skhd](https://github.com/koekeishiya/skhd) and [yabai](https://github.com/koekeishiya/yabai) is a dream to use.

I have a custom keymap layer on my keyboard just for moving windows around, and selecting which one is active. I have U/D/L/R keys on the left half of the keyboard, these are used to select which window is currently in focus. Above the Left and Right keys, I have Left and Right monitor swap keys, to swap which monitor is currently focused.

On the right half of the keyboard, I have the exact same setup, but instead of selecting focus, it actually moves the window.

A hotkey to toggle if it's a vertical or horizontal split, plus a hotkey to automatically balance all the windows on the screen, is pretty much all I ever need.

![Yabai example](yabai.gif)

## Shortcut software

I have a designated key on my keyboard to invoke [Shortcat](https://shortcat.app/). This let's me "click" on anything on the screen (or in any menus, visible or not). With my IDE open, I hit the button, and type a few letters of the project I want to open, hit enter, and there it is. With Slack open, hit the button, type a few letters of the name of who I want to message, hit enter, there's the chat window, start typing my message. This removes the other large chunk of what you would need a mouse for.

![Shortcat example](shortcat.gif)

## Mouse keys

But you can't do _everything_ with those two tools (though you can get very close) so what do I do when I actually need a mouse?

Well, the same U/D/L/R keys that I have mapped for window management, I also have mapped on a separate layer for cursor movement. Add in one button to speed up the mouse for large movements, and one to slow it down for granular control, and that's all you need. Hold down my mouse button, move the cursor to where I need with these keys, and hit either the "click" or "right-click" hot keys.

![Mouse key layout](mousekeys.png)

## Be free of the tyranny of the desk rodent
I really can't recommend this setup enough. My desk is cleaner, my arms are happier, and my work is faster. Co-workers who watch me screenshare say they can't believe I don't have a mouse, as even when I do have to actually move the cursor to do something, it's as fast as a mouse user would do it, maybe faster because I don't have to move my hands first to do so.

And if you're _really_ scared about giving it up, the keyboard I linked above offers a touchpad module. I purchased one when I purchased the keyboard "just in case", but honestly, it just ends up just collecting dust these days, as my other options of mousing are faster and easier.
