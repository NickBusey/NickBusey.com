---
title: "Connecting a Grandstream HT80X (HT801 or HT802) to Twilio"
date: 2020-01-28
categories:
  - Tutorials

---

I recently had to connect a Grandstream HT802. It's a small device that can give you an analog phone port when all you have available is ethernet ports which are connected to the internet.

It is certainly not plug and play, and is clearly intended for use in a more enterprise setting. It's what we had, and we needed it to work, so I got busy researching how to do it.

I found out Twilio offers a SIP service, and since I've used them before for both SMS and automated voice calls, I figured they would be a good place to start.

Unfortunately their documentation is a little lacking, and their support was not very helpful. That said, I got it working and thought I would share my settings to hopefully save the next unfortunate soul who has to deal with setting one of these things up some time.

That said, it was pretty simple.

## Setting up Twilio

First you need a Twilio account. Once you are registered, go to the Elastic SIP Trunking section. Create a new SIP Trunk. In our case we are only concerned with outbound calls, so we set up a Termination SIP UPI. Twilio will give you a SIP domain like: `nickstrunk.pstn.twilio.com`. They will also give you a Trunk SID that looks like: `TKa1hee4710a3f7dc421f9081bf5ee8f95`

Now you need to create a phone number in Twilio, and associate it with the SIP Trunk you just created.

### Note

When you first set up a phone number, it will be in `Trial` mode and can only dial out to `Verified Numbers` within Twilio, which should include the phone number you used to set up your account. Until you `Upgrade` to a full plan, you will not be able to call any numbers not in your `Verified Numbers` list.

## Setting up Grandstream

You should now have a SIP domain, a Trunk SID, and a phone number.

Enter your Grandstream device configuration page (default login admin/admin). Go to the `FXS PORT1` page. Enter your SIP server in `Primary SIP Server`, enter your phone number as `SIP User ID:` with the full format `(+17205551234)` and enter your Trunk SID as the `Authenticate ID`. Under `Dial Plan Prefix` enter `+` so the system will add the + that Twilio expects to be on the front of every number it receives.

Hit `Apply`.

Now, if you plug a phone into Port 1 of the your HT801/2 VOIP converter, and dial a valid phone number starting with `1`, it should start ringing the number being dialed. You're done!