---
title: "Papyrto - A simple paper based strategy game based on Quarto"
date: 2018-08-12
categories:
  - Projects
tags:
  - Games
  - Open Source
aliases:
  - /article/papyrto/

---

8 months or so ago I designed a paper adaptation of the board game,
[Quarto](https://en.wikipedia.org/wiki/Quarto_(board_game)). I call
it [Papyrto](https://gitlab.com/NickBusey/Papyrto). Papyrus (Paper) + Quarto = Papyrto.

Invented by Swiss mathematician Blaise Müller in 1991, Quarto is a 
simple game with interesting rules. There are 16 game pieces, 
each with 4 distinct attributes; tall or short, light or dark, 
round or square, and solid or hollow.

So my design had to have a similar set of 'pieces' with a different
set of 4 distinct attributes. but these attributes had to
apply to lines on paper rather than a physical shape. I ended up
with horizontal or vetical, curved or straight, dashed or solid,
and with a dot or without.

For ease of drawing them, the pieces are connected, but each of 
the 4 arms are a separate 'piece'.

![](PiecePools+GameBoard.png)

The rules are the same as Quarto. Player 1 selects a piece for Player 2 to play by drawing a circle around that piece in the piece pool. Player 2 plays the piece by drawing a copy of the same piece into an empty square on the board, then crossing out the piece in the pool. Then Player 2 circles a piece for Player 1 to play and so forth.

The first player to place a piece which results in 4 pieces in a row that all have the same of one of the 4 attributes, wins. So 4 pieces that are all horizontal, or all have dots, or all don't have dots, or are all dotted, etc.

## Example Game

Player 1 (blue ink) selects a piece for Player 2 to place. Note: different ink colors are not necessary during play. They are used here to simplify what is happening.

![](GameStep1.png)

Player 2 (green ink) places the piece, crosses out the piece they placed, and then selects a piece for Player 1 (blue ink) to place.

![](GameStep2.png)

This continues until the game ends.

![](GameStep3.png)

### Winning Board

In this board, whoever placed the last piece of the highlighted 4 pieces is the winner. All 4 pieces are curved.

![](WinningBoard.png)

