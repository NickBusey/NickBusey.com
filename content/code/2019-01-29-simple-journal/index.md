---
title: "Simplest Journal Solution"
date: 2019-01-29T20:59:17-07:00
categories:
  - Code
tags:
  - Terminal
  - Open Source
  - Journal
---

This is my simple journal solution. Put the code belowe in your `~/.zshrc` or `~/.bashrc`

Type a `j` from a terminal, type some notes, save and exit.

The script creates a file in the format of `~/journal/2019/01-29.md` and automatically pushes it to git.

```
EDITOR=vim
journal() {
    mkdir -p ~/journal/`date +%Y`
    $EDITOR ~/journal/`date +%Y`/`date +%m-%d.md`
    (cd ~/journal/; git pull; git add *; git commit -a -m "Update `date +%Y-%m-%d`"; git push)
}
alias j=journal
```

Easy, simple, effective.
