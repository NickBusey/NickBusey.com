---
title: "HomelabOS v0.6 - Release Notes and Installation Video"
date: 2019-05-31
categories:
  - Code
tags:
  - HomelabOS
  - Ansible
  - Docker
  - Open Source
  - Twitch
  - YouTube

---

Yesterday I released [v0.6 of HomelabOS](https://gitlab.com/NickBusey/HomelabOS/tags/v0.6).

HomelabOS is an effort to make it as easy as possible for anyone to host their own cloud-style
services, either at home, or on a server they control in the cloud.

The goal is to provide a few simple commands to the user, and handle setting everything
up for them as easily as possible.

It includes over 50 services that can be easily enabled, deployed, and backed up.

This latest release includes:

- Added Terraform Support - Optionally spin up cloud servers automatically
- Improved Tooling - Easily add new services not already included in HomelabOS via a Ruby script
- Improved Configuration - Added `make set` and `make get` commands to make working with the configuration easier.
- Improved documentation
- More options - More things are configurable than before
- Improved deployment - Cleaner Traefik settings
- Changed Requirements - No longer requires Ansible, only requires Docker to be installed.

I also filmed a short [installation video](https://youtu.be/p8cD349BGRI).

{{< youtube p8cD349BGRI >}}

[Tune in on Twitch](https://www.twitch.tv/nickbusey) to see live development of 
[HomelabOS](http://homelabos.com) as well as my various other projects.

[Join us in Chat](https://homelabos.zulipchat.com/) to talk about things / receive support!