---
title: "Sniffing one's own farts: Moving from GitHub to Gitlab"
date: 2018-09-04
categories:
  - Code
tags:
  - Open Source
  - Git
aliases:
  - /code/github-vs-gitlab/
---

I [recently announced](https://www.reddit.com/r/DataHoarder/comments/8qgfnx/homelabos_your_very_own_offlinefirst_opensource/)
a new side project of mine, [HomelabOS](https://gitlab.com/NickBusey/HomelabOS) on Reddit.

There was a lot of great feedback, and then there was the hilarious comment in the screenshot.

```
oh god...one of those people that moved to gitlab to 'send a message'.

OP sounds like the kind of person that sniffs their own farts
```

While I do enjoy the occassional whiff of gourmet flatulence, I thought I would address my actual motivations behind moving
my projects from [GitHub](https://github.com/NickBusey) to [GitLab](http://gitlab.com/nickbusey/).

<!--more-->

# Open Source Concerns

## GitHub

GitHub, the supposed center of all things open source, the de facto place you go to find almost any modern open source
software project, is itself in no way open source.
The entire company is built around letting these projects publish their source code, and allowing the users of those projects
to contribute with bug reports, feature requests, and of course actual code contributions. However, GitHub does
not support any of this when it comes to GitHub itself. If I wanted to help fix a problem, like the fact that for
almost [four years now](https://github.com/isaacs/github/issues/283) you still cannot view a list of the issues you are
subscribed to, I would have no way to do so. You will also note that the issue I linked to (https://github.com/isaacs/github/issues/283) is inside a repo
made by a user known as `isaacs` with the following repository description:

```
This is not the actual repository for the GitHub website.

I'm not affiliated with GitHub in any way, except that I use it all day long, and almost all my code is hosted there.

Issues and feature requests posted to GitHub are not stored in any publicly available location, so I find that I quickly lose track of the things that I've sent them.
```

That doesn't sound to me like how a company who is built around open source should operate. If they want to keep their
precious source code private, fine, but not offering even a public issue tracker is not very user friendly.

## GitLab

GitLab, on the other hand, tracks every issue (other than security/privacy sensitive issues) in the open. You are free
to comment, up/down vote it, or even just fix it your damn self by submitting a merge request.

You can watch the current pipeline of what issues the internal GitLab employees are prioritizing for the next release,
and you can take part in the discussion of the implementation of the issue before work on it ever begins.

You can easily clone the GitLab source code (with the obvious exception of their enterprise addons) right now, and
begin playing with it locally on your machine, changing how it works and experimenting with the outcomes.

You can easily and affordably self-host a GitLab instance on a single cloud server instance, and get for a very
minimal cost a private, and under your control, complete git workflow solution. [My company](http://grownetics.co)
has a private GitLab server which we use to build, test, and deploy to review apps, all of our internal docker images.

# Features

## GitHub

As noted earlier, GitHub seems to be lacking in developing either new features, or addressing old shortcomings. And
withouth any visibility into what they are working on, it can be impossible to know whether your pet issue will
ever be addressed.

Seemingly basic things for an open source platform to address, like allowing the community to nominate a fork of a project
as the new standard after the maintainer of the original project becomes inactive, still remain unaddressed.

The general lack of roadmap or communication from GitHub as a whole makes it frustrating to work with as a user who is
accustomed to being able to contribute to projects that are such a central part of their daily workflow.

## GitLab

Once again, and due in large part to the reasons already mentioned, GitLab shines brilliantly where GitHub falls short.
Not only is every issue and roadmap public, but the freedom to file your own issue, submit a merge request for that issue,
and actually have a high probability of that work seeing the light of day, changes your relationship with the tool deeply.
You are suddenly not a powerless observer, no longer just a 'consumer'. You can exert quite a bit of influence on the
project if your contributions prove themselves valuable.

# MSFT

Alright, let's address the original accusation. That I just wanted to send MSFT 'a message'.

Considering my most popular project on GitHub had 53 `stars`, I'm not quite sure I have enough pull to influence anyone at
all, let alone a company such as Microsoft.

# Conclusion

I moved my projects from GitHub to GitLab because my projects are open source, and I prefer the platform they are
distributed on be open source as well.

