---
title: "Introducing tckr.html - A simple calculator strip. Like ticker tape, with variables."
date: 2020-01-28
categories:
  - Code
tags:
  - Open Source
  - JavaScript

---

I've been wanting a tool lately where it functions kind of like a blank text document,
but when you type some math it actually evaluates the formula for you, letting you save
the output for use in further equations.

So, last night I built it over the course of maybe two hours.

It's simple. It does what it says on the box. It's portable (just one .html file). It's easy.

[Hosted Version](https://nickbusey.gitlab.io/tckr.html/)

[Download tckr.html](https://gitlab.com/NickBusey/tckr.html)

[JSFiddle Demo](https://jsfiddle.net/7kn0rqbf/)

![Demo Gif](tckrdemo.gif)