---
title: "tor_ssh.sh - One command to enable SSH access via Tor to any Ubuntu server."
date: 2019-03-01
categories:
  - Code
tags:
  - Tor
  - SSH
  - Grownetics
  - Open Source
  - Ubuntu
---

{{< youtube iib2nG0mvfo >}}

At [Grownetics](http://grownetics.co/) we install onsite servers at our clients' facilities so they can continue to use the system, even if their internet goes out.

These servers have fail over connections, and may change connection at any time, and due to the onsite nature of things, we may not be able to have ports forwarded or expect there
to be a static IP.

In these cases a VPN is a nice thing to have, and we use [Tinc](https://tinc-vpn.org/) for this. It works great, but it does require a cloud node to be accessible, and for various
reasons that cloud node link can stop working. When it does, we could potentially lose access to these onsite servers, requiring a site visit to regain connectivity. This is less than ideal,
and this is where Tor comes in.

Using [Tor](https://www.torproject.org/) hidden services, it is fairly trivial to set up an ssh access point that can be connected to pretty much as long as the server is powered and online.

We use [Ansible](https://www.ansible.com/) to automatically set Tor up for us on our new server deployments. However during development and testing it can be nice to have a way to gain
fast remote access to a server without needing to deal with Ansible.

So I created [tor_ssh.sh](https://gitlab.com/grownetics/devops/blob/master/tor_ssh.sh) to do just that. All you need to do to use it is run:

```
bash <(curl -s https://gitlab.com/grownetics/devops/raw/master/tor_ssh.sh)
```

and it will spit out an onion address like tmxybgr6e7kpenoq.onion that you can use to connect to the node from any other machine.
