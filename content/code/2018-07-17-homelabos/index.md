---
title: "Introducing HomelabOS - Ansible scripts to deploy privacy centric personal servers "
date: 2018-07-17
categories:
  - Code
tags:
  - HomelabOS
  - Ansible
  - Docker
  - Open Source

---

I've been working on a new Open Source project lately called [HomelabOS](http://homelabos.com) that aims to make it easy to set up a home server to be a nearly complete cloud services replacement.

I call it 'Your very own offline-first privacy-centric open-source data-center!'

The goal is to make it easy for anyone to own all their data in an easy and secure way, without the need of cloud providers.

It has a simple one-command setup (`make`) that uses Ansible to configure and deploy dozens of services for you in Docker containers to a server in your home network.

# Features

* One command deployment
* Automated Backups
* Easy Restore
* Automated Apple Health Data import
* Automated Tor Onion Service access
* Automated HTTPS via LetsEncrypt
* OpenVPN

# Software Included

* [Bitwarden](https://bitwarden.com/) - Password and secrets manager via [bitwarden-rs](https://github.com/dani-garcia/bitwarden_rs)
* [Butterfly](https://github.com/paradoxxxzero/butterfly) - Web based terminal access
* [Convos](https://convos.by/) - Always-on IRC client (IRC bouncer)
* [Darksky](http://darksky.net/) - Local weather reported via [darksky-influxdb](https://github.com/ErwinSteffens/darksky-influxdb)
* [Dasher](https://github.com/maddox/dasher) - Amazon Dash button support
* [Documentation](https://nickbusey.gitlab.io/HomelabOS/) - Offline, searchable documentation via [MkDocs](https://www.mkdocs.org/)
* [Emby](https://emby.media/) - Media player
* [Firefly III](https://firefly-iii.org/) - Money management budgeting app
* [Gitea](https://gitea.io/en-US/) - Git hosting
* [Grafana](https://grafana.com/) - Pretty graphs
* [Home Assistant](https://www.home-assistant.io/) - Home Automation
* [InfluxDB](https://www.influxdata.com/time-series-platform/influxdb/) - Time series data storage
* [Mastodon](https://joinmastodon.org/) - Federated social microblogging
* [Matomo](https://matomo.org/) - Web analytics
* [Minio](https://minio.io/) - S3 hosting
* [NextCloud](https://nextcloud.com/) - Private Cloud Storage, Calendar, Contacts, etc.
* [Organizr](https://github.com/causefx/Organizr) - Access all your HomelabOS services in one easy place.
* [Paperless](https://github.com/danielquinn/paperless) - Document management
* [Pi-hole](https://pi-hole.net/) - Ad blocking
* [Portainer](https://www.portainer.io/) - Easy Docker management
* [Telegraf](https://www.influxdata.com/time-series-platform/telegraf/) - Server statistics reporting
* [Transmission](https://transmissionbt.com/) - BitTorrent client

# Future

I plan to migrate the deployment from a docker-compose file to a more modular Kubernetes setup.

# Download

[Latest releases](https://gitlab.com/NickBusey/HomelabOS/tags)

Let me know if you try it out and how it goes!