---
title: "Installing and configuring network wide adblock with Pi-hole and the Turris Omnia."
date: 2016-12-04T22:12:21-06:00
categories:
  - Code
tags:
  - Pihole
  - Turris
  - Omnia
  - Raspberry Pi
  - Networking
  - Adblock
  - Open Source
aliases:
  - /installing-and-configuring-network-wide-adblock-with-pi-hole-and-the-turris-omnia/

---

# NOTICE: This article is outdated. Using LXC containers on the native flash memory will wear it down quickly. Look into [HomelabOS](http://homelabos.com/) for your piHole needs.

My new Turris Omnia arrived on Saturday morning, and it took me almost a full day to get Pi-hole setup and configured, so I thought I’d save someone else the time.

<!--more-->

<iframe width="560" height="315" src="https://www.youtube.com/embed/4IYAROVCbU0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

It is pretty straightforward, but there are a few 'gotchas' along the way.

* Setup your Turris Omnia, and connect to it’s network. You should be able to load the LuCI interface, and access the internet.
* Inside LuCI go to Services -> LXC Containers. Create a new container called ‘pi-hole’ and select Ubuntu Xenial. Edit the container, and add the following as a new line: lxc.start.auto = 1
* Make sure to start the container.
* SSH into your Omnia. $ ssh root@192.168.1.1 should be all you need. Use the password you set when you first set your router up.
* Now go into your new LXC container $ lxc-attach -n pi-hole
* Install Pi-hole
 * I had some trouble using the ‘curl’ command they provide on the website. I had to copy/paste the script contents into install.sh, $ chmod +x install.sh, then execute it with $ ./install.sh
 * Make note of the IP that is displayed.
 * You may need to symlink pihole to the /usr/bin. $ cd /usr/bin; ln -s ../local/bin/pihole pihole
 * You may also need to manually update pihole’s block lists with: $ pihole -g
* Test Pi-hole
 * You should be able to set the DNS of your machine to point to the IP of your pi-hole instance, then load a site with a bunch of ads, and navigate to http://*YOUR_PIHOLE_IP*/admin/queries.php to see the queries that were blocked. If things look good, remove the DNS mapping on your machine, load the site with ads again, and you should see the ads this time.
* Configure Turris Omnia
 * Now it’s time to tell the router to apply the DNS at it’s level rather than the machine level so you don’t have to edit the DNS settings of every device you connect.
 * In LuCI under System -> Startup, ‘Disable’ and ‘Stop’ both the ‘resolver’ and ‘kresd’ initscripts. ‘Enable’ the ‘lxc-auto’ script.
 * Under Network -> Interfaces, click ‘Edit’ on WAN, then ‘Advanced Settings’. Uncheck ‘Use DNS servers advertised by peer’ and in the ‘Use custom DNS servers’ field that appears, enter the IP of the Pi-hole instance that you saved earlier.
 * SSH into your Omnia again and inside /etc/config/dhcp change option port ‘0’ to option port ’53’.
  * Edit /etc/config/lxc-auto to match this:
```
          config container
          option name pi-hole
          option timeout 60
```
  * Copy this file https://gitlab.com/NickBusey/pihole-led/raw/master/pihole-led.sh to /root/pihole-led.sh
  * Run `crontab -e`, add a new line: `@reboot /root/pihole-led.sh`
* Reboot your Turris Omnia.

Congratulations! If everything went well, you should now be automatically blocking ads for any device that is connected to your Turris Omnia router, and the User 1 LED should be flashing purple with each blocked ad.
