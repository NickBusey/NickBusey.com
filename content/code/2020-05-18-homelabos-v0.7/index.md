---
title: "HomelabOS v0.7 - 50+ new services! Release notes and installation video"
date: 2020-05-18
categories:
  - Code
tags:
  - HomelabOS
  - Ansible
  - Docker
  - Open Source
  - Twitch
  - YouTube

---

Today we released [v0.7 of HomelabOS](https://gitlab.com/NickBusey/HomelabOS/-/releases)!

> HomelabOS is an effort to make it as easy as possible for anyone to host their own cloud-style services, either at home, or on a server they control in the cloud.
>
> The goal is to provide a few simple commands to the user, and handle setting everything up for them as easily as possible.

Since the last release we have added over 50 more services, for [over 100 services](https://homelabos.com/docs/#available-software) total!

This release also includes:

- Added Encrypted Secrets - All secrets stored in the settings repo are now automatically encrypted with Ansible Vault. Use `make decrypt` to see the values in the file. It re-encrypts when you run `make`.
- Added One-Line Deploy - `bash <(curl -s https://gitlab.com/NickBusey/HomelabOS/-/raw/dev/install_homelabos.sh)`
- Updated everything to use Traefik v2
- Auto-populating Dashboard w/ SUI - Any services you enable are detected and added to this dashboard.

[Full release notes](https://gitlab.com/NickBusey/HomelabOS/-/releases)

When I say 'we' I mean the over 40 contributors who have been helping out!

Kevin Poorman ([@codefriar](https://gitlab.com/codefriar)) and Denis Evers (@[denis.ev](https://gitlab.com/denis.ev)) are definitely our MVPs this release, and have both been made Maintainers which means they can approve MRs and merge MRs when they gain enough approvals. Everyone is now working through MRs (even me), and all MRs require at least two approvals before being merged. We will continue to add maintainers and build a governance structure as things continue to evolve.

I want to take one moment out and say I'm blown away by the response the project has gotten recently. Many great people are participating in the chat, working on the code, and improving the documentation. Thank you to everyone!

There is also a new [HomelabOS website](https://homelabos.com/).

I recorded a short [installation and overview video](https://youtu.be/lbmViEFTj4o).

{{< youtube lbmViEFTj4o >}}

[Tune in on Twitch](https://www.twitch.tv/nickbusey) to see live development of 
[HomelabOS](http://homelabos.com) as well as my various other projects.

[Join us in Chat](https://homelabos.zulipchat.com/) to talk about things / receive support. We have over 300 members in chat!

The [HomelabOS subreddit](https://reddit.com/r/HomelabOS/) has over 500 members as well.

Finally, you can follow the project directly on [Twitter @HomelabOS](https://twitter.com/HomelabOS)