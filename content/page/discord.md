---
title: "Discord"
# This absurd date is so the Bilberry theme sorts the header nav items correctly
date: 1985-07-02

# set the link if you want to redirect the user.
link: "https://discord.gg/vRZbjKdsQ5"
# set the html target parameter if you want to change default behavior
target: "_blank"
---
