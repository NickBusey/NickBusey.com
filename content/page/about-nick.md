---
title: "About Nick"
date: 1986-07-02
author: "Nick Busey"
excludeFromTopNav: false
---
# Who

My name is Nick Busey.

# What

I am the CTO and co-founder of [Grownetics](https://grownetics.co).

I contribute to [several open source projects](https://gitlab.com/NickBusey/)
which I work on regularly on my [Twitch stream](https://www.twitch.tv/nickbusey),
and post tutorials about to my [YouTube channel](https://www.youtube.com/user/NickBusey).

I am available for consulting. Find my contact info below.

I also [make and DJ music](http://gangsterish.com), ride BMX bikes and motorcycles, snowboard, skateboard, wakeboard, and other such tomfoolery.

# Where

Boulder, CO

# Why

Entirely self taught, I’ve had a passion for technology since making my first website in fourth grade. I’ve been freelancing since middle school and have been working in the Boulder startup community since 2006.

# How

Chat me on Keybase at [nickbusey](https://keybase.io/nickbusey). Email me at nickabusey on Google’s email service, send me a text message at (303) 532-5670, or hit me up on Twitter [@NickBusey](https://twitter.com/NickBusey) or Mastodon [@NickBusey@sdf.mastodon.org](https://mastodon.sdf.org/@nickbusey).