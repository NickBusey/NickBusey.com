serve:
	docker run -p 1313:1313 -v ${PWD}/static:/static -v ${PWD}/themes:/themes -v ${PWD}/config.toml:/config.toml -v ${PWD}/content:/content registry.gitlab.com/pages/hugo:0.53 hugo serve --bind 0.0.0.0
